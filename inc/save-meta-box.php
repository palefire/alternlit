<?php
function alt_lit_save_mbx_value( $post_id ){

	$recommendation_amz_link = isset($_REQUEST['recommendation_amz_link'])?trim($_REQUEST['recommendation_amz_link']):"";
	if(!empty($recommendation_amz_link)){
		update_post_meta($post_id,"recommendation_amz_link",$recommendation_amz_link);
	}else{
		update_post_meta($post_id,"recommendation_amz_link","");	
	}
	$recomendation_author = isset($_REQUEST['recomendation_author'])?trim($_REQUEST['recomendation_author']):"";
	if(!empty($recomendation_author)){
		update_post_meta($post_id,"recomendation_author",$recomendation_author);
	}else{
		update_post_meta($post_id,"recomendation_author","");	
	}

}

add_action("save_post","alt_lit_save_mbx_value");