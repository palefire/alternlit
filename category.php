<?php get_header(); ?>

<div class="unknown-fifth " id="4" >
		<section class="first-section">
			<!-- <div class="shadow"></div> -->
			<div class="content">
				<h4 class="title">Explore the world of the past curated by the <br>voices of our future.</h4>
				<div class="author">Alternate Literature.</div>
			</div>
		</section>
		<section class="second-section">
			<div class="row">
				<?php
					
					while( have_posts() ){
						the_post();
						$categories = get_the_category();
				?>
					<div class="col-md-4">
						<div class="topic">
							<?php 
				               if( has_post_thumbnail() ){
				                 the_post_thumbnail('altlit-normal');
				               }else{
				                 ?>
				            		<img src="https://alternativeliterature.com/wp-content/uploads/2021/01/image-placeholder.jpg" alt="AlternativeLiterarture">
				            <?php
				               	}
				               ?>
							<p> 
								<?php 
									if($categories){
										foreach($categories as $category){
								?>
											<a href="<?php echo get_category_link($category->term_id) ?>"><span><?php echo 	$category->cat_name ?></span></a>
								<?php
								}
									}
								?>
							</p>
							
							<h5><a href="<?php echo get_permalink(); ?>"><?php echo get_the_title(); ?></a></h5>
						</div>
					</div>
				<?php 
					}
					wp_reset_postdata();
				?>
				
			</div>
		</section>
	</div>
<?php get_footer(); ?>