<?php
	get_header();
?>
	<div class="unknown-first " id="">
		<section class="first-section">
			<div class="content">
				<div class="left-side">
					<div class="top-one">2021</div>
					<div class="midle-one"><img src="<?php echo get_template_directory_uri().'/assets/images/whitelogo.png' ?>"></div>
					<div class="bottom-one">Engage in alternative ways of seeing, perceiving and analysing the world.</div>
				</div>
				<div class="right-side">
					<div class="content" style="background-image: url(<?php echo get_template_directory_uri().'/assets/images/HOMEPAGE_IMAGE.png' ?>);background-repeat: no-repeat; background-position: center; background-size: cover;">
						
					</div>
				</div>
			</div>
		</section>
	</div>
	<!-- Féministe -->
	<div class="unknown-fifth " id="4" >
		<section class="first-section" style="background-image: url('<?php echo get_template_directory_uri().'/assets/images/feministe.png' ?>')">
			<!-- <div class="shadow"></div> -->
			<div class="content">
				<h4 class="title">Féministe</h4>
				<div class="author"><a href="https://alternativeliterature.com/category/feministe/">Read More.</a></div>
			</div>
		</section>
		<section class="second-section">
			<div class="row">
				<div class="col-12">
					<div></div>
				</div>
			</div>
			<div class="row">
				<?php
					$args = array(
						'posts_per_page'      => 2,
						'category_name'       => 'feministe',
					);
					$the_query = new WP_Query( $args );
					while( $the_query->have_posts() ){
						$the_query->the_post();
						$categories = get_the_category();
						$post_id = get_the_ID();
						$post_tags = get_the_tags($post_id);
						$date_format= get_option('date_format');
	  					$postDate='<label>'.get_the_time($date_format).'</label>';
	  					$author_url = get_author_posts_url(get_the_author_meta('ID'));
				?>
					<div class="col-md-6 col-12">
						<div class="topic">
							<?php 
				               if( has_post_thumbnail() ){
				                 the_post_thumbnail('altlit-normal');
				               }else{
				                 ?>
				            		<img src="https://alternativeliterature.com/wp-content/uploads/2021/01/image-placeholder.jpg" alt="AlternativeLiterarture">
				            <?php
				               	}
				               ?>
							<p> 
								<?php 
									if($post_tags){
										foreach($post_tags as $tag){

								?>
											<a href="#" class="no-click-a"><span><?php echo $tag->name ?></span></a>
								<?php
								}
									}
								?>
							</p>
							
							<h5><a href="<?php echo get_permalink(); ?>"><?php echo get_the_title(); ?></a></h5>
							<p class="inside-topic" style=""><?php echo wp_trim_words( get_the_content(), 55, '' ); ?></p>
							<h4><?php echo get_the_author(); ?></h4>
						</div>
					</div>
					
				<?php 
					}
					wp_reset_postdata();
				?>
				
			</div>
		</section>
	</div>
	<!-- //Féministe -->

	<!-- Columns -->
	<div class="unknown-fifth " id="4" >
		<section class="first-section" style="background-image: url('<?php echo get_template_directory_uri().'/assets/images/columns.png' ?>')">
			<!-- <div class="shadow"></div> -->
			<div class="content">
				<h4 class="title">Columns</h4>
				<div class="author"><a href="https://alternativeliterature.com/category/columns/">Read More.</a></div>
			</div>
		</section>
		<section class="second-section">
			<div class="row">
				<div class="col-12">
					<div></div>
				</div>
			</div>
			<div class="row">
				<?php
					$args = array(
						'posts_per_page'      => 3,
						'category_name'       => 'columns',
					);
					$the_query = new WP_Query( $args );
					while( $the_query->have_posts() ){
						$the_query->the_post();
						$categories = get_the_category();
						$post_id = get_the_ID();
						$post_tags = get_the_tags($post_id);
						$date_format= get_option('date_format');
	  					$postDate='<label>'.get_the_time($date_format).'</label>';
	  					$author_url = get_author_posts_url(get_the_author_meta('ID'));
				?>
					<div class="col-md-4 col-12">
						<div class="topic">
							<?php 
				               if( has_post_thumbnail() ){
				                 the_post_thumbnail('altlit-normal');
				               }else{
				                 ?>
				            		<img src="https://alternativeliterature.com/wp-content/uploads/2021/01/image-placeholder.jpg" alt="AlternativeLiterarture">
				            <?php
				               	}
				               ?>
							<p> 
								<?php 
									if($post_tags){
										foreach($post_tags as $tag){

								?>
											<a href="#" class="no-click-a"><span><?php echo $tag->name ?></span></a>
								<?php
								}
									}
								?>
							</p>
							
							<h5><a href="<?php echo get_permalink(); ?>"><?php echo get_the_title(); ?></a></h5>
							<p class="inside-topic" style=""><?php echo wp_trim_words( get_the_content(), 25, '' ); ?></p>
							<h4>By <span><?php echo get_the_author(); ?></span></h4>
						</div>
					</div>
				<?php 
					}
					wp_reset_postdata();
				?>
				
			</div>
		</section>
	</div>
	<!-- //Columns -->

	<!-- Ficciones -->
	<div class="unknown-tenth">
		<section class="first-section" style="background-image: url('<?php echo get_template_directory_uri().'/assets/images/ficciones.png' ?>')">
			<!-- <div class="shadow"></div> -->
			<div class="content">
				<h4 class="title">Ficciones</h4>
				<div class="author"><a href="https://alternativeliterature.com/category/ficciones/">Read More</a></div>
			</div>
		</section>
		<section class="second-section">
			<div class="row">
				<?php
					$args = array(
						'posts_per_page'      => 3,
						'category_name'       => 'ficciones',
					);
					$the_query = new WP_Query( $args );
					while( $the_query->have_posts() ){
						$the_query->the_post();
						$categories = get_the_category();
						$post_id = get_the_ID();
						$post_tags = get_the_tags($post_id);
						$date_format= get_option('date_format');
	  					$postDate='<label>'.get_the_time($date_format).'</label>';
	  					$author_url = get_author_posts_url(get_the_author_meta('ID'));
				?>
				<div class="col-md-6">
					<a href="#">
						<h1><?php echo get_the_title(); ?></h1>
					</a>
					
					<p><?php echo get_the_content(); ?></p>

					<h4> <span><?php echo get_the_author(); ?></span><?php echo $postDate; ?></h4>
				</div>
				<?php 
					}
					wp_reset_postdata();
				?>
			</div>
			
		</section>
	</div>
	<!-- //Ficciones -->

	<!-- Black Screen -->
	<div class="unknown-second " id="1"  >
		<section class="first-section">
			<div class="content">
				<p class="firstpstyle">When she does not find love, she may find poetry. Because she does not act, she observes, she feels, she records; a color, a smile awakens profound echoes within her; her destiny is outside her, scattered in cities already built, on the faces of men already marked by life, she makes contact, she relishes with passion and yet in a manner more detached, more free, than that of a young man.</p>
				<p class="secondpstyle">Simone de Beauvoir, The Second Sex</p>
			</div>
		</section>
	</div>
	<!-- //Black Screen -->

	<!-- Poetry -->
	<div class="unknown-tenth">
		<section class="first-section" style="background-image: url('<?php echo get_template_directory_uri().'/assets/images/poetry.png' ?>')">
			<!-- <div class="shadow"></div> -->
			<div class="content">
				<h4 class="title">Poetry</h4>
				<div class="author"><a href="https://alternativeliterature.com/category/poetry/">Read More</a></div>
			</div>
		</section>
		<section class="second-section">
			<div class="row">
				<?php
					$args = array(
						'posts_per_page'      => 2,
						'category_name'       => 'poetry',
					);
					$the_query = new WP_Query( $args );
					while( $the_query->have_posts() ){
						$the_query->the_post();
						$categories = get_the_category();
						$post_id = get_the_ID();
						$post_tags = get_the_tags($post_id);
						$date_format= get_option('date_format');
	  					$postDate='<label>'.get_the_time($date_format).'</label>';
	  					$author_url = get_author_posts_url(get_the_author_meta('ID'));
				?>
				<div class="col-md-6">
					<a href="#">
						<h1><?php echo get_the_title(); ?></h1>
					</a>
					
					<p><?php echo get_the_content(); ?></p>

					<h4> <span><?php echo get_the_author(); ?></span><?php echo $postDate; ?></h4>
				</div>
				<?php 
					}
					wp_reset_postdata();
				?>
			</div>
			
		</section>
	</div>
	<!-- //Poetry -->

	<!-- Content -->
	<div class="unknown-seventh">
		<section class="first-section">
			<div class="left-side">
				<div class="content">Contents</div>
			</div>
			<div class="right-side">
				<a href="https://alternativeliterature.com/category/ficciones/" class="topic">
					<h2>Ficciones</h2>
				</a>
				<a href="https://alternativeliterature.com/category/columns/" class="topic">
					<h2>Columns</h2>
				</a>
				<a href="https://alternativeliterature.com/category/feministe/" class="topic">
					<h2>Féministe</h2>
				</a>
				<a href="https://alternativeliterature.com/category/poetry/" class="topic">
					<h2>Poetry</h2>
				</a>
				<a href="https://alternativeliterature.com/category/poetry/" class="topic">
					<h2>Subscribe</h2>
				</a>
				<a href="https://alternativeliterature.com/about/" class="topic">
					<h2>About</h2>
				</a>
			</div>
		</section>
	</div>
	<!-- //Content -->

	<!-- Foot -->
	<div class="unknown-sixth">
		<section class="first-section">
			<div class="left-side">
				<div class="content">
					<h6 class="top-title">Drop us a line if you're interested in getting involved.</h6>
					<p class="top-head">If it be sponsorship, contribution, stocklist or other,<br> we would love to hear from you.</p>
					<p class="topa-auth">hola@alternativeliterature.com</p>
					<p class="bottom-topic">We are always on the lookout for the voices of our generation. If our work interests you, yours might interest us.<br><br> <a href="https://alternativeliterature.com/wp-content/uploads/2021/01/Submission-Guidelines-©-AlternativeLiterature.pdf" target="_blank" style="color: #DE543F">Click here</a> for the detailed submission guidelines. Submissions are open all year round. We promise to keep our response times short and get back to you even if your work doesn't make the cut. 
					For announcements, contests, and rewards, subscribe to our newsletter. </p> 
				</div>
			</div>
			<div class="right-side">
				<div class="content">
					<div class="topic">
						<h6>PUBLISHED BY</h6>
						<li><a href="https://thepalefire.com/" target="_blank">Palefire</a></li>
					</div>
					<div class="topic">
						<h6>SOCIAL SHARE</h6>
						<!-- <li><a href="#">FACEBOOK</a></li> -->
						<li><a href="https://www.instagram.com/alternativeliterature/" target="_blank">INSTAGRAM</a></li>
					</div>
					<!-- <div class="topic">
						<h6>SUBSCRIBE</h6>
					</div>
					<div class="topic">
						<h6>CONTACT US</h6>
					</div> -->
					<div class="topic">
						<script type="text/javascript" language="javascript">
					      var aax_size='300x250';
					      var aax_pubname = 'alternativ0ed-21';
					      var aax_src='302';
					    </script>
					    <script type="text/javascript" language="javascript" src="http://c.amazon-adsystem.com/aax2/assoc.js"></script>
					</div>
				</div>
			</div>
		</section>
	</div>
	<!-- //Foote -->

	

	<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script> -->
  	<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script> -->
  	<!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script> -->
<?php
	get_footer();
?>