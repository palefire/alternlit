<?php get_header(); ?>

<div class="unknown-tenth">
	<section class="first-section" style="background-image: url('<?php echo get_template_directory_uri().'/assets/images/ficciones.png' ?>')">
		<!-- <div class="shadow"></div> -->
		<div class="content">
			<h4 class="title">Ficciones</h4>
			
		</div>
	</section>
	<section class="second-section">
		<div class="row">
			<?php
				while( have_posts() ){
					the_post();
					$categories = get_the_category();
					$post_id = get_the_ID();
					$post_tags = get_the_tags($post_id);
					$date_format= get_option('date_format');
  					$postDate='<label>'.get_the_time($date_format).'</label>';
  					$author_url = get_author_posts_url(get_the_author_meta('ID'));
			?>
			<div class="col-md-6">
				<a href="#">
					<h1><?php echo get_the_title(); ?></h1>
				</a>
				
				<p><?php echo get_the_content(); ?></p>

				<h4> <span><?php echo get_the_author(); ?></span><?php echo $postDate; ?></h4>
			</div>
			<?php 
				}
				
			?>
		</div>
		
	</section>
</div>
<?php get_footer(); ?>