<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<link href="https://fonts.googleapis.com/css2?family=Roboto:wght@100;300;400;500;700;900&display=swap" rel="stylesheet">
	<?php
		wp_head();
	?>
	<style type="text/css">
		
	</style>
	<header class="desk-header">
		<div class="left-head">
			<a href="<?php echo home_url(); ?>" class="logo">alternative <span>literature</span></a>
		</div>
		<div class="right-head">
			<div class="right-head-down">
				<ul class="right-head-down-ul">
					<!-- <li>
						<span class="right-head-down-span">
							<a href="">RECOMMENDATION</a>
						</span>
					</li> -->
					<li>
						<span class="right-head-down-span">
							 <div class="dropdown margin-bottom">
	        					<a href="#" class="dropdown-toggle" data-toggle="dropdown">CATEGORIES</a>
	        					<div class="dropdown-menu">
	            					<a href="https://alternativeliterature.com/category/ficciones/" class="dropdown-item drop-a">Ficciones</a>
	            					<a href="https://alternativeliterature.com/category/columns/" class="dropdown-item drop-a">Columns</a>
	            					<a href="https://alternativeliterature.com/category/feministe/" class="dropdown-item drop-a">Féministe</a>
	            					<a href="https://alternativeliterature.com/category/poetry/" class="dropdown-item drop-a">Poetry</a>
	        					</div>
	   						 </div>
						</span>
					</li>
					<li>
						<span class="right-head-down-span">
							 <div class="dropdown margin-bottom">
	        					<a href="#" class="dropdown-toggle" data-toggle="dropdown">RECOMMENDATION</a>
	        					<div class="dropdown-menu">
	            					<!-- <a href="#" class="dropdown-item drop-a">Cinemas</a> -->
	            					<a href="https://alternativeliterature.com/book-recommendation/" class="dropdown-item drop-a">Books</a>
	        					</div>
	   						 </div>
						</span>
					</li>
					<li>
						<span class="right-head-down-span">
							<a href="">ABOUT</a>
						</span>
					</li>
					<!-- <li>
						<span class="right-head-down-span">
							<a href="">TOURS</a>
						</span>
					</li> -->
					<div class="clearfix"></div>
				</ul>
				<div class="clearfix"></div>
			</div>
		</div>
		
		<div class="clearfix"></div>
	</header>
	<header class="mobile-header">
		<nav class="navbar navbar-expand-lg navbar-light bg-light">
		  <a href="<?php echo home_url(); ?>" class="logo">alternative <span>literature</span></a>
		  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
		    <span class="navbar-toggler-icon"></span>
		  </button>

		  <div class="collapse navbar-collapse" id="navbarSupportedContent">
		    <ul class="navbar-nav mr-auto">
		      <li class="nav-item dropdown">
		        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
		          CATEGORIES
		        </a>
		        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
		          <a href="https://alternativeliterature.com/category/ficciones/" class="dropdown-item drop-a">Ficciones</a>
				  <a href="https://alternativeliterature.com/category/columns/" class="dropdown-item drop-a">Columns</a>
				  <a href="https://alternativeliterature.com/category/feministe/" class="dropdown-item drop-a">Féministe</a>
				  <a href="https://alternativeliterature.com/category/poetry/" class="dropdown-item drop-a">Poetry</a>
		        </div>
		      </li>
		      <li class="nav-item dropdown">
		        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
		          RECOMMENDATION
		        </a>
		        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
		          <a href="https://alternativeliterature.com/book-recommendation/" class="dropdown-item drop-a">Books</a>
		        </div>
		      </li>
		      <li>
			  <li class="nav-item">
        		<a class="nav-link" href="#">ABOUT</a>
      		  </li>
			</li>
		    </ul>
		  </div>
		</nav>
	</header>
	<div class="header-pad">
	<!-- <div class="mobile-head">
			<i class="fas fa-bars"></i>
	</div> -->
</head>
<body <?php body_class(); ?>>

