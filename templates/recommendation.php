<?php
  /*
  Template Name: Recommendation
  Template Post Type: page
  */
  	get_header();
  	?>
  	<div class="unknown-nine">
		<section class="first-section">
			<div class="head-top">
				<h1>Read the Books we Read</h1>
				<div class="line"></div>
			</div>
			<div class="content">
				<div class="row">
					<?php
						$args = array(
							// 'posts_per_page'      => 4,
							'post_type' => 'alt_recommendation'
						);
						$the_query = new WP_Query( $args );
						while( $the_query->have_posts() ){
							$the_query->the_post();
							$categories = get_the_category();
							$post_id = get_the_ID();
							$post_tags = get_the_tags($post_id);
							$date_format= get_option('date_format');
		  					$postDate='<label>'.get_the_time($date_format).'</label>';
		  					$author_url = get_author_posts_url(get_the_author_meta('ID'));
		  					$recommendation_amz_link = get_post_meta($post->ID,"recommendation_amz_link",true);
		  					$recomendation_author = get_post_meta($post->ID,"recomendation_author",true);
					?>
					<div class="col-md-3 col-12">
						<a href="<?php echo $recommendation_amz_link ?>" target="_blank">
							<div class="topic">
								<div class="img-box">
									<?php 
						               if( has_post_thumbnail() ){
						                 the_post_thumbnail('altlit-normal');
						               }else{
						                 ?>
						            		<img src="https://alternativeliterature.com/wp-content/uploads/2021/01/image-placeholder.jpg" alt="AlternativeLiterarture">
						            <?php
						               	}
						               ?>
								</div>
								<div class="item"><p><?php echo get_the_title(); ?></p></div>
								<h4><?php echo $recomendation_author ?></h4>
							</div>
						</a>
					</div>
					<?php 
						}
						wp_reset_postdata();
					?>
					
				</div>
			</div>
		</section>
	</div>
	<?php
    get_footer();
?>