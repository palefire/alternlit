<?php
  /*
  Template Name: Poetry
  Template Post Type: post
  */
  get_header();
  while( have_posts() ){
  	the_post();
  	$post_id = get_the_ID();
  	$date_format= get_option('date_format');
  	$postDate='<label>'.get_the_time($date_format).'</label>';
  	$content = get_the_content();
  	$author_url = get_author_posts_url(get_the_author_meta('ID'));
  	$author_name = '<label><i class="tgicon usr"></i><a href="'. $author_url.'" class="post-author">'.get_the_author().'</a></label>';
  	 
    // $author_post = get_post_meta(get_the_ID(),"tg_author_meta_value",true);
?>
<div class="unknown-eight">
	<section class="first-section">
		<div class="content">
			<h2 class="heading"><?php the_title() ?></h2>
			<h6 class="second-heading"><?php echo get_the_author(); ?></h6>
			<p class="word"><?php echo $postDate ?></p>
			<!-- <p class="author">Sakir Ali</p> -->
			<div class="all-item"><?php the_content(); ?></div>
		</div>
	</section>
</div>
<?php 
}//while
get_footer(); 
?>