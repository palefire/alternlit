<?php
	get_header();
	while( have_posts() ){
  		the_post();
		$post_id = get_the_ID();
	  	$date_format= get_option('date_format');
	  	$postDate='<label>'.get_the_time($date_format).'</label>';
	  	$content = get_the_content();
	  	$author_url = get_author_posts_url(get_the_author_meta('ID'));
	  	$author_name = '<label><i class="tgicon usr"></i><a href="'. $author_url.'" class="post-author">'.get_the_author().'</a></label>';
	  	$get_img = get_the_post_thumbnail_url($post_id,'altlit-normal');
?>
	
	<div class="unknown-third " id="2" >
		<section class="first-section">
			<div class="left-side">
				<div class="left-content">
					<h6>WRITTEN BY</h6>
					<p><?php echo get_the_author(); ?></p>
				</div>
				<div class="right-content">
					<h6>PUBLISHED ON</h6>
					<p><?php echo $postDate ?></p>
				</div>
			</div>
			<div class="right-side" style="background-image: url(<?php echo $get_img;?>)"></div>
			<div class="midle-content"><?php the_title() ?></div>
		</section>
	</div>
	<div class="unknown-fourth " id="3" >
		<section class="first-section">
			<div class="left-side">
				<h2 class="title"><?php the_title() ?></h2>
				<p class="author"><?php echo get_the_author(); ?></p>
			</div>
			<div class="midle-side">
				<!-- <h6 class="content-title">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</h6> -->
				<div class="all-content">
					<?php the_content(); ?>
				</div>
				
			</div>
			<div class="right-side" ></div>
			<!-- <div class="bottom-arrow">&#8595;</div> -->

		</section>
	</div>
	
<?php
}//while
	get_footer();
?>