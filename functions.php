<?php
function alternlit_setup(){
	add_theme_support( 'title-tag' );


	add_theme_support(
		'post-formats',
		array(
			'link',
			'aside',
			'gallery',
			'image',
			'quote',
			'status',
			'video',
			'audio',
			'chat',
		)
	);

	add_theme_support( 'post-thumbnails' );
	add_image_size('altlit-normal', 675, 592 );
	add_image_size('recomendation-normal', 395, 592 );

	register_nav_menus(
		array(
			'primary' => esc_html__( 'Primary menu', 'alternlit' ),
			'footer'  => __( 'Secondary menu', 'alternlit' ),
		)
	);

	add_theme_support(
		'html5',
		array(
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
			'style',
			'script',
			'navigation-widgets',
		)
	);
}
add_action( 'after_setup_theme', 'alternlit_setup' );




function alternlit_scripts(){
	wp_enqueue_style( 'alternlit-style', get_template_directory_uri() . '/style.css', array(), microtime() );
	wp_enqueue_style( 'alternlit-style-responsive', get_template_directory_uri() . '/assets/css/responsive.css', array(), microtime() );

	wp_enqueue_script( 'popper', 'https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js', array('jquery'), '1.14.7', false );
	wp_enqueue_script( 'bootstrap', 'https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js', array('jquery'), '4.3.1', false );

	wp_enqueue_script( 'alternlit', get_template_directory_uri().'/assets/js/altenlit.js', array('jquery'), microtime(), false );
}

add_action( 'wp_enqueue_scripts', 'alternlit_scripts' );

function alternlit_admin_scripts( $hook ){
	wp_enqueue_media();
	wp_register_script('alternlit-script',get_template_directory_uri().'assets/js/altenlit.js',array('jquery'),microtime(), true);
	wp_enqueue_script('alternlit-script');
}
add_action('admin_enqueue_scripts','alternlit_admin_scripts');

//=======================custom user details==========================

add_action( 'show_user_profile', 'extra_user_profile_fields' );
add_action( 'edit_user_profile', 'extra_user_profile_fields' );

function extra_user_profile_fields( $user ) { ?>
    <h3><?php _e("Extra profile information", "blank"); ?></h3>

    <table class="form-table">
    <tr>
        <th><label for="facebook"><?php _e("Facebook"); ?></label></th>
        <td>
            <input type="text" name="facebook" id="facebook" value="<?php echo esc_attr( get_the_author_meta( 'facebook', $user->ID ) ); ?>" class="regular-text" /><br />
            <span class="description"><?php _e("Please enter your facebook."); ?></span>
        </td>
    </tr>
    <tr>
        <th><label for="twitter"><?php _e("Twitter"); ?></label></th>
        <td>
            <input type="text" name="twitter" id="twitter" value="<?php echo esc_attr( get_the_author_meta( 'twitter', $user->ID ) ); ?>" class="regular-text" /><br />
            <span class="description"><?php _e("Please enter your twitter."); ?></span>
        </td>
    </tr>
    <tr>
    	<th><label ><?php _e("Profile Picture"); ?></label></th>
		<td>
			
		
    		<input type = "button" class="button button secondary" value = "Upload Profile Picture" id = "upload-button">
			<input type="button" class="button button-secondary" value="Remove " id="remove-picture">
    		<input type = "hidden" id="profile-picture" name = "profile_picture" value="<?php echo esc_attr( get_the_author_meta( 'profile_picture', $user->ID ) ); ?>"  />
			<br>
			<span class="description"><?php _e("Please click update after uploading."); ?></span>
		</td>

    </tr>
  
    </table>
<?php }


add_action( 'personal_options_update', 'save_extra_user_profile_fields' );
add_action( 'edit_user_profile_update', 'save_extra_user_profile_fields' );

function save_extra_user_profile_fields( $user_id ) {
    if ( !current_user_can( 'edit_user', $user_id ) ) { 
        return false; 
    }
    update_user_meta( $user_id, 'facebook', $_POST['facebook'] );
    update_user_meta( $user_id, 'twitter', $_POST['twitter'] );
    update_user_meta( $user_id, 'profile_picture', $_POST['profile_picture'] );
  
}



//=======================Custom Post Type Recommendation==========================
require get_template_directory(). '/inc/custom_post_type.php';
require get_template_directory(). '/inc/meta-box.php';
require get_template_directory(). '/inc/save-meta-box.php';
 
