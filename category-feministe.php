<?php get_header(); ?>

<div class="unknown-fifth " id="4" >
	<section class="first-section" style="background-image: url('<?php echo get_template_directory_uri().'/assets/images/feministe.png' ?>')">
		<!-- <div class="shadow"></div> -->
		<div class="content">
			<h4 class="title">Féministe</h4>
			
		</div>
	</section>
	<section class="second-section">
		<div class="row">
			<div class="col-12">
				<div></div>
			</div>
		</div>
		<div class="row">
			<?php
				
				while( have_posts() ){
					the_post();
					$categories = get_the_category();
					$post_id = get_the_ID();
					$post_tags = get_the_tags($post_id);
					$date_format= get_option('date_format');
  					$postDate='<label>'.get_the_time($date_format).'</label>';
  					$author_url = get_author_posts_url(get_the_author_meta('ID'));
			?>
				<div class="col-md-6 col-12">
					<div class="topic">
						<?php 
			               if( has_post_thumbnail() ){
			                 the_post_thumbnail('altlit-normal');
			               }else{
			                 ?>
			            		<img src="https://alternativeliterature.com/wp-content/uploads/2021/01/image-placeholder.jpg" alt="AlternativeLiterarture">
			            <?php
			               	}
			               ?>
						<p> 
							<?php 
								if($post_tags){
									foreach($post_tags as $tag){

							?>
										<a href="#" class="no-click-a"><span><?php echo $tag->name ?></span></a>
							<?php
							}
								}
							?>
						</p>
						
						<h5><a href="<?php echo get_permalink(); ?>"><?php echo get_the_title(); ?></a></h5>
						<p class="inside-topic" style=""><?php echo wp_trim_words( get_the_content(), 55, '' ); ?></p>
						<h4><?php echo get_the_author(); ?></h4>
					</div>
				</div>
				
			<?php 
				}
				
			?>
			
		</div>
	</section>
</div>
<?php get_footer(); ?>